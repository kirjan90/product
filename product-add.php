<!doctype html>
<html lang="en">
  <head>
    <title>Product Add</title>
                                <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

                                <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="product-add.css">
  </head>
  <body>
                                <!----- Header ----->
    <div class="container jumbotron">
        <header>
            <div class="container">
                <div class="row">
                <div class="logo col-sm-6">
                    <h1>Product Add</h1>
                </div>
                <div class="buttons col-sm-6">
                    <input class="btn btn-success" name="save" type="submit" value="Save" form="product-form">
                    <a class="btn btn-danger" href="product-list.php">Cancel</a>
                </div>
                </div>
            </div>
        </header>
        <hr>
                                <!----- Form ----->
        <div class="container">
            <form action="action.php" method="post" id="product-form">
                <div class="form-group">
                    <label for="sku">SKU</label>
                    <input class="form-control" type="text" id="sku" name="sku" required    >
                </div>
                <div class="form-group">
                    <label for="name">Name</label>
                    <input class="form-control" type="text" name="name" id="name" required>
                </div>
                <div class="form-group">
                    <label for="price">Price($)</label>
                    <input class="form-control" type="number" name="price" id="price" required>
                </div>
                <div class="form-group">
                    <label for="type">Type</label>
                    <select class="form-control" name="type" onchange="changeOptions(this)" id="props" required>
                        <option value="" selected="selected">Select type</option>
                        <option value="form_1">CD</option>
                        <option value="form_2">Book</option>
                        <option value="form_3">Furniture</option>
                    </select>
                </div>
                                <!----- Sub Form ----->
                <div class="sub-form form-group" id="form_1">
                    <hr>
                    <h5 class="text-muted text-center">Please provide size</h5>
                    <label for="size">Size(MB)</label>
                    <input type="number" class="form-control" name="dvd" id="size">
                </div>
                <div class="sub-form form-group" id="form_2">
                    <hr>
                    <h5 class="text-muted text-center">Please provide weigth</h5>
                    <label for="book">Weigth(KG)</label>
                    <input type="number" class="form-control" name="book" id="book">
                </div>
                <div class="sub-form form-group" id="form_3">
                    <hr>
                    <h5 class="text-muted text-center">Please provide dimensions</h5>
                    <label for="heigth">Heigth(CM)</label>
                    <input type="number" class="form-control" name="furniture[]" id="heigth">
                    <label for="width">Width(CM)</label>
                    <input type="number" class="form-control" name="furniture[]" id="width">
                    <label for="length">Length(CM)</label>
                    <input type="number" class="form-control"name="furniture[]" id="length">
                </div>
            </form>
        </div>
        <hr>
    </div>
                                <!-- Optional JavaScript -->
    <script>
       function changeOptions(selectEl) {
            let selectedValue = selectEl.options[selectEl.selectedIndex].value;
            let subForms = document.getElementsByClassName('sub-form')
            
            for (let i = 0; i < subForms.length; i += 1) {
                let input = subForms[i].querySelectorAll("input");
                
                if (selectedValue === subForms[i].id) {
                subForms[i].setAttribute('style', 'display:block');
                    for(let i =0; i < input.length; i++){
                        input[i].setAttribute('required','');
                    }
                } else {
                subForms[i].setAttribute('style', 'display:none')
                    for(let i =0; i < input.length; i++){
                        input[i].removeAttribute('required')
                    }
                }
            }
        }
    </script>
                        <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
  </body>
</html>