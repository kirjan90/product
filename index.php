<!doctype html>
<html lang="en">
  <head>
    <title>Product List</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

                                <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="product-list.css">
  </head>
  <body>
      <?php include 'action.php';
            include 'connection.php';
      ?>
                                <!----- Header ----->
    <div class="container jumbotron">
      <header>
            <div class="container">
                <div class="row">
                <div class="logo col-sm-6">
                    <h1>Product List</h1>
                </div>
                <div class="buttons col-sm-6">
                    <a href="product-add.php" class="btn btn-success">Add</a>
                    <input class="btn btn-danger" type="submit" name="delete" value="Mass Delete" form="check-form">
                </div>
                </div>
            </div>
        </header>
                                <!----- Main section ----->
        <div class="container main">
        <hr>
        <div>
            <h3 class="h3 text-muted">DVD</h3>
            <hr>
        </div>
        <form action="" method="post" id="check-form">
        <div class="row">
        <?php while($row = $result->fetch_assoc()){?>
            <div class="col-2" id=card>
                <input type="checkbox" name="checkbox[]" id ="check" class="checkbox" value="<?= $row["id"]; ?>" >
                <div><?= $row['sku'].$row['id']; ?></div>
                <div><?= $row['name']; ?></div>
                <div><?= $row['price']." $"; ?></div>
                <div><?= "Size: ".$row['size']." MB"; ?></div>
            </div>
        <?php } ?>
        </div>
        
        <hr>
        
        <div>
            <h3 class="h3 text-muted">Books</h3>
            <hr>
        </div>
        <div class="row">
        <?php while($row = $result1->fetch_assoc()){?>
            <div class="col-2" id=card>
                <input type="checkbox" name="checkbox[]" id ="check" class="checkbox" value="<?= $row["id"]; ?>" >
                <div><?= $row['sku'].$row['id']; ?></div>
                <div><?= $row['name']; ?></div>
                <div><?= $row['price']." $"; ?></div>
                <div><?= "Weight: ".$row['weight']." KG"; ?></div>
            </div>
        <?php } ?>
        </div>

        <hr>
        
        <div>
            <h3 class="h3 text-muted">Furniture</h3>
            <hr>
        </div>
        <div class="row">
        <?php while($row = $result2->fetch_assoc()){?>
            <div class="col-2" id=card>
                <input type="checkbox" name="checkbox[]" id ="check" class="checkbox" value="<?= $row["id"]; ?>" >
                <div><?= $row['sku'].$row['id']; ?></div>
                <div><?= $row['name']; ?></div>
                <div><?= $row['price']." $"; ?></div>
                <div><?= "Dimensions: ".$row['heigth']."x".$row['width']."x".$row['length']." CM"; ?></div>
            </div>
        <?php } ?>
        </div>
      </div>
      </form>
    </div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
  </body>
</html>